<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('halaman.daftar');
    }
    
    public function send(Request $request)
    {
        //dd($request->all());
        $fname = $request["fname"];
        $lname = $request["lname"];
        $gender = $request["gender"];
        $negara = $request["negara"];
        $bio = $request["bio"];

        return view('halaman.home', compact('fname','lname','gender','negara','bio'));
    }
}
